<?php
	include_once '../backend/includes/helpers.php';
	include_once '../names.php';
	if (file_exists('../searchbase.php')) {
        include_once '../searchbase.php';
    }
	// Custom ETag Handling
	$etag = '"' . md5_file(__FILE__) . (isset($_GET['frame']) ? 'F"' : '"');
	if (array_key_exists('HTTP_IF_NONE_MATCH', $_SERVER) && trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) {
		header('HTTP/1.1 304 Not Modified');
		die();
	}
	header('ETag: ' . $etag);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Video List</title>
		<meta charset="UTF-8">
		<base target="_parent">
		<link rel="stylesheet" type="text/css" href="../CSS/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../CSS/page.css">
		<link rel="stylesheet" type="text/css" href="../CSS/list.css">
<?php if(isset($_GET['frame'])) echo '		<link rel="stylesheet" type="text/css" href="../CSS/frame.css">'; ?>
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="../JS/lang.js"></script>
		<script src="../JS/list.js"></script>
		<script type="text/javascript">
			// Set config values from PHP into JavaScript.
			window.config = {
				VIDEO_INDEX_PADDING: <?php echo $VIDEO_INDEX_PADDING; ?>,
				USE_FILENAME_AS_IDENTIFIER: <?php echo $USE_FILENAME_AS_IDENTIFIER ? 'true' : 'false'; ?>,
				ADD_KARAOKE_INFO_LINK: <?php echo $ADD_KARAOKE_INFO_LINK ? 'true' : 'false'; ?>,
				USE_UID_AS_IDENTIFIER: <?php echo $USE_UID_AS_IDENTIFIER ? 'true' : 'false'; ?>
			};
		</script>
	</head>
	<body>
		<header>
			<div>
				<h1>Video List</h1>
				<?php include '../hub/navbar'; ?>
			</div>
		</header>
		<main>
            <div id="playlist" hidden>
                <p class="playlistTop">
                    <span id="playlist-checkbox">
                        <i class="fa fa-chevron-right"></i>
                        <i class="fa fa-chevron-down"></i>
                    </span>
                    <span id="playlistTitle">
                        <?php echo I18N::t('{number} Videos in Playlist', ['{number}' => 0]) ?>
                    </span>
                </p>
                <div class="playlistContent">

                </div>
                <p class="playlistBot">
                    <span><?php echo I18N::t('Edit Playlist') ?></span><span class="split"></span>
                    <span><?php echo I18N::t('Start Playlist') ?></span>
                    <?php if ($USE_UID_AS_IDENTIFIER) { ?><span class="split"></span><span><?php echo I18N::t('Export Playlist') ?></span><?php } ?>
                </p>
            </div>

			<?php
			// Count videos/series
			// Remove 18+ if they weren't requested.
			if (!isset($_GET['r18'])) {
				foreach ($names as $series => $video_array) {
					foreach ($video_array as $title => $data)
						if (isset($data['r18']) && $data['r18'])
							unset($names[$series][$title]);
					if (!$names[$series])
						unset($names[$series]);
				}
			}

			$videosnumber = 0;
			foreach ($names as $videos) $videosnumber += count($videos);

			$seriesnumber = count($names);

            echo '<p>'.I18N::t('We currently serve {videos} videos from {series} series.', [
                    '{videos}' => '<span class="count">' . $videosnumber . '</span>',
                    '{series}' => '<span class="count">' . $seriesnumber . '</span>'])
                .'</p>';
            ?>

		<label>
			<a id="searchURL" href=""><?php echo I18N::t('Search:') ?></a>
			<form name="fmSearch" action="" method="get" target="<?php if(isset($_GET['frame'])) echo('_ifSearch'); else echo('_self'); ?>">
			<?php if(isset($_GET['frame'])) echo('<input type="hidden" name="frame" value="true">'); ?>
			<?php if(isset($_GET['r18'])) echo('<input type="hidden" name="r18" value="true">'); ?>
			<input name="s" id="searchbox" type="text" placeholder="<?php echo I18N::t('Series or titles name...') ?>" value="<?php if(isset($_GET['s'])) echo($_GET['s']) ?>" autofocus>
			</form>
		</label>
		<br>
		<p id="regex"><span>(<?php echo I18N::t('Press Enter or wait 3 seconds to start the search'); ?>)</span></p>
		<br>
		<?php

			$hasResults = false;
			$searchCriteria = '';

			if(!isset($_GET['frame']))
				$searchCriteria = '§*§'; //We have some titles with one or more'*' in them.
			if(isset($_GET['s']) && $_GET['s'] !== '') {
                $transliterator = Transliterator::createFromRules(':: NFD; :: [:Nonspacing Mark:] Remove; :: Lower(); :: NFC;', Transliterator::FORWARD);
                $searchCriteria = $transliterator->transliterate($_GET['s']);
            }

			if($searchCriteria !== '') {
				// Output list of videos

				foreach ($names as $series => $video_array)
				{
					$html = '';
                    /** @var array $searchbase - This is an array if ../searchbase.php loads successfully */
                    if (isset($searchbase)) {
                        foreach ($searchbase[$series] as $name => $search) {

                            // Easter eggs are not in the search base
                            // Skip r18 if not defined
                            if(!isset($_GET['r18']) && strpos($search, 'r18') !== false) continue;

                            if ($searchCriteria === '§*§' || strpos($search, $searchCriteria) !== false) {
                                $data = $names[$series][$name];
                                // Generate HTML for each video
                                $html .= '	<i class="fa fa-plus" data-file="' . htmlspecialchars($data['file']) . '" data-mime="' . htmlspecialchars(json_encode($data['mime'])) . '"';
                                if (array_key_exists('song', $data)) $html .= ' data-songtitle="' . htmlspecialchars($data['song']['title']) . '" data-songartist="' . htmlspecialchars($data['song']['artist']) . '" data-songwriters="'.htmlspecialchars(json_encode($data['song']['songwriters'])).'"';
                                if (array_key_exists('subtitles', $data)) $html .= ' data-subtitles="' . htmlspecialchars($data['subtitles']) . '"';
                                if ($USE_UID_AS_IDENTIFIER) $html .= ' data-uid="' . htmlspecialchars($data['uid']). '" ';
                                $html .= '></i>' . PHP_EOL;
                                if ($USE_UID_AS_IDENTIFIER)
                                    $html .= '	<a href="../?video=' . filenameToIdentifier($data['uid']) . (isset($_GET['r18']) ? '&r18' : '') . '">' . $name . '</a>' . PHP_EOL;
                                else
                                    $html .= '	<a href="../?video=' . filenameToIdentifier($data['file']) . (isset($_GET['r18']) ? '&r18' : '') . '">' . $name . '</a>' . PHP_EOL;
                                $html .= '	<br>' . PHP_EOL;
                            }
                        }
                    } else {
                        foreach ($video_array as $title => $data)
                        {
                            // Skip Easter Eggs
                            if (isset($data['egg']) && $data['egg']) continue;
                            // Skip r18 if not defined
                            if(!isset($_GET['r18']) && isset($data['r18'])) continue;

                            if(
                                $searchCriteria === '§*§'
                                || (stripos(iconv('UTF-8', 'ASCII//TRANSLIT',$series), $searchCriteria) !== false)
                                || (stripos(iconv('UTF-8', 'ASCII//TRANSLIT',$data['song']['title']), $searchCriteria) !== false)
                                || (stripos(iconv('UTF-8', 'ASCII//TRANSLIT',$data['song']['artist']), $searchCriteria) !== false)
                                || (stripos(iconv('UTF-8', 'ASCII//TRANSLIT',$data['subtitles']), $searchCriteria) !== false)
                            ) {
                                // Generate HTML for each video
                                $html .= '	<i class="fa fa-plus" data-file="' . htmlspecialchars($data['file']) . '" data-mime="' . htmlspecialchars(json_encode($data['mime'])) . '"';
                                if (array_key_exists('song', $data)) $html .= ' data-songtitle="' . htmlspecialchars($data['song']['title']) . '" data-songartist="' . htmlspecialchars($data['song']['artist']) . '"';
                                if (array_key_exists('subtitles', $data)) $html .= ' data-subtitles="' . htmlspecialchars($data['subtitles']) . '"';
                                if ($USE_UID_AS_IDENTIFIER) $html .= ' data-uid="' . htmlspecialchars($data['uid']). '" ';
                                $html .= '></i>' . PHP_EOL;
                                if ($USE_UID_AS_IDENTIFIER)
                                    $html .= '	<a href="../?video=' . filenameToIdentifier($data['uid']) . (isset($_GET['r18']) ? '&r18' : '') . '">' . $title . '</a>' . PHP_EOL;
                                else
                                    $html .= '	<a href="../?video=' . filenameToIdentifier($data['file']) . (isset($_GET['r18']) ? '&r18' : '') . '">' . $title . '</a>' . PHP_EOL;
                                $html .= '	<br>' . PHP_EOL;
                            }
                        }
                    }

					// If any video data HTML was generated, output the series name and the HTML
					if ($html) {
						echo '<div class="series">' . $series . '<div>' . PHP_EOL;
						echo $html;
						echo '</div></div>' . PHP_EOL;
						$hasResults = true;
					}
				}
			}

			if($hasResults === false) {
				?>
				<div id="NoResultsMessage">
				<p><?php echo I18N::t('We could not find any shows matching your search query.') ?></p>
				<ol>
					<li><?php echo I18N::t('Is it spelled correctly?') ?></li>
					<li><?php echo I18N::t('Have you tried using the Japanese title?') ?></li>
					<li><?php echo I18N::t('Have you tried using the English title?') ?></li>
				</ol>
				<p><?php echo I18N::t('If you still can\'t find the video you are looking for, we probably don\'t have it yet.') ?></p>
                <p><a href="https://kara.moe/base/karas?p=0&filter=<?php echo urlencode($_GET['s']) ?>&order=recent" target="_blank"><?php echo I18N::t('Click here to suggest a karaoke for our base!') ?></a></p>
			</div>
			<?php
			}
			?>
		</main>

        <?php $jsctl = I18N::_('js')->dump();
        if(!empty($jsctl)) {
            echo '<template id="locale">'.json_encode($jsctl).'</template>';
        } ?>
		<?php include_once '../backend/includes/botnet.html'; ?>
	</body>
</html>
