let AudioBackgroundManager = {
    isAudioOnly: function() {
        let r = true;
        for (let mime of Videos.list[Videos.index].mime) {
            if(mime.startsWith('video/')) {
                r = false;
            }
        }
        return r;
    },
    /**
     * Because currentSrc won't be loaded, and cannot be premised
     */
    getTrustedSource: function() {
        let r = null;
        for(let c of document.getElementById('bgvid').children) {
            if(c.hasAttribute('src') && c.type.startsWith('audio/')) {
                r = c.src;
            }
        }
        return r;
    },
    auto: function() {
        if(AudioBackgroundManager.isAudioOnly()) {
            let trustedSource = null;
            if(window.config.AUDIO_COVERS_BACKGROUND && (null !== (trustedSource = AudioBackgroundManager.getTrustedSource()))) {
                fetch(trustedSource) // load by using the current supported SRC
                    .then(response => response.blob()) // swap as blob promise
                    .then((response) => {
                        const reader = new FileReader(); // straight from MP3Tag doc
                        reader.onload = function () {
                            const mp3tag = new MP3Tag(this.result);
                            mp3tag.read();
                            let bgPictureFound = false;
                            for(let frame of mp3tag.frames) {
                                if(frame.id.endsWith('PIC')) { // PIC / APIC
                                    const bgImg = "url('data:"+frame.value.format+";base64, "+AudioBackgroundManager.base64(frame.value.data)+"')";
                                    document.getElementById('wrapper').style.backgroundImage = bgImg;
                                    bgPictureFound = true;
                                }
                            }
                            if(!bgPictureFound) {
                                document.getElementById('wrapper').style.backgroundImage = window.config.DEFAULT_BACKGROUND_IMAGE;
                            }
                            document.getElementById('wrapper').style.backgroundSize = 'contain'; // show it
                        }

                        reader.readAsArrayBuffer(response);
                    });
            } else {
                document.getElementById('wrapper').style.backgroundSize = 'contain'; // show it
            } // no trusted source, ignore, prolly bottleneck case
        } else {
            document.getElementById('wrapper').style.backgroundSize = '0px'; // hide it
        }
    },
    base64: function(buffer) {
        let binary = '';
        let bytes = new Uint8Array( buffer );
        let len = bytes.byteLength;
        for (let i = 0; i < len; i++) {
            binary += String.fromCharCode( bytes[ i ] );
        }
        return window.btoa( binary );
    }
};
