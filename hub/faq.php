<!DOCTYPE html>
<html>
	<head>
		<title>F.A.Q.</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="../CSS/page.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="../CSS/font-awesome.css">
		<style>
			.keycap {
				border: 1px solid rgba(0, 0, 0, 0.2);
				background-color: rgba(0, 0, 0, 0.05);
				padding: 2px;
			}
		</style>
	</head>
	<body>
		<header>
			<div>
				<h1>Frequently Asked Questions</h1>
				<?php include 'navbar'; ?>
			</div>
		</header>
		<main>
			<p>French version below. Version française plus bas.</p>

			<h2>English version</h2>

			<h3 id="why-wont-the-videos-play">Why won't the videos play?</h3>

			<p>If you're using a new version of Chrome, you will need to click the page first to make it start. Chrome has decided to block autoplaying videos to stop spam, which unfortunately also breaks our site.</p>

			<p>If you believe another issue is causing this, contact <a href="https://twitter.com/Kuwaddo/">Quad on Twitter</a> or alternatively by mail (<a href="mailto:contact@quad.moe">contact@quad.moe</a>) and we can try to work out a solution.</p>

			<h3 id="why-do-none-of-the-buttons-work">Why do none of the buttons work?</h3>

			<p>You have disabled JavaScript. Don't do that.</p>

			<h3 id="i-got-this-really-weird-video">I got this really weird video, what does that mean?</h3>

			<p>There are Easter Eggs hidden on the site. Congratulations, you found one!</p>

			<h3 id="keybindings">Can I use my keyboard to interact with the site?</h3>

			<p>Yes. These are the current keybindings on the home page:</p>

			<p><span class="keycap">M</span> Open/Close menu</p>
			<p><span class="keycap">/</span> Open search pane</p>
			<p><span class="keycap">N</span> New video</p>
			<p><span class="keycap">S</span> Toggle subtitles (if available)</p>
			<p><span class="keycap"><span class="fa fa-arrow-left"></span>/<span class="fa fa-arrow-right"></span></span> Back/Forward 10 seconds</p>
			<p><span class="keycap">Space</span> Pause/Play</p>
			<p><span class="keycap">F</span> Toggle fullscreen</p>
			<p><span class="keycap">Page Up/Down or Scroll Wheel</span> Volume</p>

			<h3>Why aren't all the videos listed?</h3>
			
			<p>Videos rated 18+ are not accessible by default. To display them, simply add <b>&r18</b> in the address bar or click <a href="/?r18">here</a></p>

			<h2>Version française</h2>

			<h3 id="why-wont-the-videos-play-fr">Pourquoi les vidéos ne s'affichent pas?</h3>

			<p>Si vous utilisez une version récente de Google Chrome, vous devez cliquer dans la page pour démarrer la vidéo. Chrome a décidé de bloquer la lecture automatique pour bloquer le spam, ce qui, malheureusement bloque également notre site.</p>
			
			<p>Si vous pensez qu'il s'agit d'un autre problème, contactez <a href="https://twitter.com/Kuwaddo/">Quad sur Twitter</a> (en anglais) ou par email (<a href="mailto:contact@quad.moe">contact@quad.moe</a>) (également en anglais) et nous essaierons de trouver une solution.</p>

			<h3 id="why-do-none-of-the-buttons-work-fr">Pourquoi les boutons ne fonctionnent pas ?</h3>

			<p>Vous avez désactivé javascript. Ne le faites pas.</p>

			<h3 id="i-got-this-really-weird-video-fr">J'ai eu cette vidéo vraiment bizarre, qu'est-ce que ça veut dire ?</h3>

			<p>Il y a des <i>Easter Eggs</i> (lit. œufs de pâques) cachés sur le site. Félicitations, vous en avez trouvé un !</p>

			<h3 id="keybindings">Puis-je utiliser mon clavier pour interagir avec le site ?</h3>

			<p>Bien entendu. Ces raccourcis claviers fonctionnent sur la page d'accueil :</p>

			<p><span class="keycap">M</span> Ouvrir/fermer le menu</p>
			<p><span class="keycap">/</span> Ouvrir le panneau de recherche</p>
			<p><span class="keycap">N</span> Nouvelle vidéo</p>
			<p><span class="keycap">S</span> Afficher/masquer les sous-titres (si disponibles)</p>
			<p><span class="keycap"><span class="fa fa-arrow-left"></span>/<span class="fa fa-arrow-right"></span></span> Reculer/avancer de 10 secondes</p>
			<p><span class="keycap">Space</span> Pause/Lecture</p>
			<p><span class="keycap">F</span> Afficher en plein écran</p>
			<p><span class="keycap">Page haut/bas ou molette de la souris</span> Régler le volume</p>

			<h3>Pourquoi toutes les vidéos ne sont pas listées ?</h3>

			<p>Les vidéos classées 18+ ne sont pas accessibles par défaut. Pour les afficher, il suffit d'ajouter <b>&r18</b> dans la barre d'adresse ou de cliquer <a href="/?r18">ici</a></p>
		</main>

		<?php include_once '../backend/includes/botnet.html'; ?>
	</body>
</html>
