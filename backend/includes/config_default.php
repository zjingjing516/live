<?php
// This file contains the default values for all of the available settings.
// By ensuring they are all set here, we don't have check that they are set before using them.
// To overwrite these values, create a copy of this file named 'config.php' in this directory.

// The base URL of this website (ex. 'openings.moe'). If this is not set in 'config.php' it will
// return an HTTP error and die.
$WEBSITE_URL = '';

// This specifies the number of digits that the index number in video filenames is padded to.
$VIDEO_INDEX_PADDING = 2;
// If true, this specifies that the name of the video file should be used as its URL identifier.
$USE_FILENAME_AS_IDENTIFIER = false;
// If true, this adds a link to the karaoke page located on the KM website.
$ADD_KARAOKE_INFO_LINK = false;
// If true, this specifies that the unique identifier of the video file should be used as its URL identifier.
$USE_UID_AS_IDENTIFIER = false;

// The e-mail addresses to send video submissions to.
$SUBMISSION_EMAIL_TO = '';
// The https://www.mailgun.com/ URL, with API key, to send e-mail to.
$MAILGUN_URL = '';
// The https://www.mailgun.com/ e-mail address to use.
$MAILGUN_EMAIL = '';

// Number of songs between two jingles/break cards. 0 to disable .
// 20 songs: about 30 minutes of openings/endings .
// Requires a directory named "jingles" with mp4 videos.
$JINGLES_INTERVAL = 0;

/**
 * List available languages. Ensure files for translations are in proper directory
 */
$APPLANGS = ['en', 'fr', 'pl',];

/**
 * If lang is not asked by user or not detected, this will be used.
 */
$DEFAULT_APPLANG = 'en';

/**
 * Default background image, used for audio-only
 * Can virtually supports remote URI, but be wary about http/https mixes
 */
$DEFAULT_BACKGROUND_IMAGE = '';

/**
 * Try to detect audio only covers to display those in background
 */
$AUDIO_COVERS_BACKGROUND = true;

?>
